using System.Collections.Generic;
using Klak.Ndi;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace nyc.crux.vjx.scenes
{
    public class SubSceneStub: MonoBehaviour
    {
        public RenderTexture RenderTexture;
        public Texture2D ThumbnailTexture;
        public NdiSender NdiSender;
        public int Index;
        public Dictionary<string, string> MidiControls;

        public void Init(RenderTexture renderTexture, Texture2D thumbnailTexture, int index, Dictionary<string, string> midiControls)
        {
            RenderTexture = renderTexture;
            ThumbnailTexture = thumbnailTexture;
            Index = index;
            MidiControls = midiControls;
            Init();
        }

        public void Init()
        {
            Graphics.Blit(ThumbnailTexture, RenderTexture);
            
            NdiSender = GetComponent<NdiSender>();
            NdiSender.sourceTexture = RenderTexture;
        }
    }
}