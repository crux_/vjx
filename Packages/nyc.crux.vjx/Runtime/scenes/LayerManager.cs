using nyc.crux.vjx.enums;
using nyc.crux.vjx.extensions;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

namespace nyc.crux.vjx.scenes
{
    public class LayerManager
    {
        public void SetLayer(Scene scene, Layers layer)
        {
            var rootObjects = scene.GetRootGameObjects();
            var intLayer = (int) layer;

            foreach (GameObject rootObject in rootObjects)
            {
                DisableAudioListeners(rootObject);
                CullCameras(rootObject, intLayer);
                CullLights(rootObject, intLayer);
                rootObject.SetLayerRecursively(intLayer);
            } 
        }

        void DisableAudioListeners(GameObject obj)
        {
            var audioListeners = obj.GetComponentsInChildren<AudioListener>();
            foreach (var audioListener in audioListeners)
            {
                audioListener.enabled = false;
            }
        }

        void CullCameras(GameObject obj, int layer)
        {
            var cameras = obj.GetComponentsInChildren<Camera>();
            foreach (var camera in cameras)
            {
                camera.cullingMask = 1 << layer;
            }
            
            
            var hdCameras = obj.GetComponentsInChildren<HDAdditionalCameraData>();
            foreach (var hdCamera in hdCameras)
            {
                hdCamera.volumeLayerMask = (1 << layer | 1 << 0);
            }
        }

        void CullLights(GameObject obj, int layer)
        {
            var lights = obj.GetComponentsInChildren<Light>();
            foreach (var light in lights)
            {
                light.cullingMask = 1 << layer;
            }
        }
    }
}