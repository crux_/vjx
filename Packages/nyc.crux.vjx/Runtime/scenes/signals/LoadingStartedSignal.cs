namespace nyc.crux.vjx.scenes.signals
{
    public class LoadingStartedSignal
    {
        public LoadingStartedSignal(int totalSceneCount)
        {
            TotalSceneCount = totalSceneCount;
        }

        public int CurrentSceneIndexLoading { get; set; }

        public int TotalSceneCount
        {
            get; set;
        }
    }
}