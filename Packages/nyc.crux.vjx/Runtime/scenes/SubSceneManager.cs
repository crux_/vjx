﻿using System.Collections;
using System.Collections.Generic;
using Minis;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

namespace nyc.crux.vjx.scenes
{
    public class SubSceneManager : MonoBehaviour
    {
        [SerializeField]
        public Camera[] cameras;
        private RenderTexture _targetTexture;
        
        private readonly WaitForSeconds _frameEnd = new WaitForSeconds(0.1f);
        private Texture2D _screenShot;

        private PlayerInput _playerInput;
        
        public virtual void Setup(RenderTexture targetTexture)
        {
            _targetTexture = targetTexture;

            foreach (var camera in cameras)
            {
                camera.targetTexture = targetTexture;

            }
        }

        public virtual void InitializeInput(int layer)
        {
            if (_playerInput == null)
            {
                _playerInput = GetComponent<PlayerInput>();
            }

            if (_playerInput != null)
            {
                AssignPlayerInputMap(layer);
            }
        }

        void AssignPlayerInputMap(int layer)
        {
            if (MidiDevice.current != null)
            {
                _playerInput.SwitchCurrentControlScheme("MIDI", MidiDevice.current);
                _playerInput.SwitchCurrentActionMap("Layer"+layer+"Map");
                Debug.Log(_playerInput.currentActionMap);
            }
        }

        public Texture2D GetScreenShot()
        {
            return _screenShot;
        }

        public IEnumerator CreateScreenShot()
        {
            yield return _frameEnd;
            RenderTexture.active = _targetTexture;
            _screenShot = new Texture2D(_targetTexture.width, _targetTexture.height, TextureFormat.RGB24, true);
            _screenShot.ReadPixels(new Rect(0, 0, _targetTexture.width, _targetTexture.height), 0, 0, false);
            _screenShot.Apply();
        }
                
        public virtual Dictionary<string, string> GetMidiControls()
        {
            return new Dictionary<string, string>();
        }
    }
}
