using System;
using System.Collections.Generic;
using System.Linq;
using Klak.Ndi;
using nyc.crux.vjx.data;
using nyc.crux.vjx.enums;
using nyc.crux.vjx.scenes.signals;
using nyc.crux.vjx.ui.signals;
using UniRx.Async;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace nyc.crux.vjx.scenes
{
    public class MainSceneManager: MonoBehaviour
    {
        [SerializeField] 
        private NdiResources ndiResources;
        
        private Dictionary<string, string> _compositionIndexToSceneName;
        private Dictionary<string, SubSceneStub> _sceneNameToStub;
        
        private LayerManager _layerManager;
        private ResolumeCompositionLoader _resolumeCompositionLoader;
        private SignalBus _signalBus;

        private Dictionary<string, Scene> _loadedScenes; 
        
        [Inject]
        void Init(LayerManager layerManager, ResolumeCompositionLoader resolumeCompositionLoader, SignalBus signalBus)
        {
            _layerManager = layerManager;
            _resolumeCompositionLoader = resolumeCompositionLoader;
            _signalBus = signalBus;
            _signalBus.Subscribe<ResolumePathSubmittedSignal>(LoadCompositionData);
        }

        void Start()
        {
            _sceneNameToStub = new Dictionary<string, SubSceneStub>();
            _loadedScenes = new Dictionary<string, Scene>();
            InitSubSceneStubs().Forget();
        }

        void LoadCompositionData(ResolumePathSubmittedSignal signal)
        {
            _compositionIndexToSceneName = _resolumeCompositionLoader.LoadComposition(signal.ResolumeCompositionPath);
            var stubSceneNames = _sceneNameToStub.Keys.ToList();
            _compositionIndexToSceneName = _compositionIndexToSceneName.Where(kvp => stubSceneNames.Contains(kvp.Value)).ToDictionary(i => i.Key, i => i.Value);
            _signalBus.Fire(new CompositionLoadedSignal());
        }

        async UniTask InitSubSceneStubs()
        {
            var currentlyLoadingSignal = new LoadingStartedSignal(SceneManager.sceneCountInBuildSettings-1);
            var subSceneStubs = new GameObject("SubSceneStubs");
            
            for (var i = 1; i < SceneManager.sceneCountInBuildSettings; i++)
            {
                currentlyLoadingSignal.CurrentSceneIndexLoading = i;
                _signalBus.Fire(currentlyLoadingSignal);
                var stub = await InitSceneStub(i);
                stub.transform.parent = subSceneStubs.transform;
            }
            
            _signalBus.Fire(new LoadingFinishedSignal());
        }


        async UniTask<GameObject> InitSceneStub(int i)
        {
            //load scene and set to thumbnail layer
            await SceneManager.LoadSceneAsync(i, LoadSceneMode.Additive);
            var scene = SceneManager.GetSceneByBuildIndex(i);
            _layerManager.SetLayer(scene, Layers.ThumbnailMaker);
            
            //get subscenemanager from scene
            SubSceneManager manager = FindSubSceneManager(scene);
            
            //create stubobj
            var stubObj = CreateSubSceneStub(scene.name);
                
            var rt = GetRenderTexture(i);  
                        
            manager.Setup(rt);
            await manager.CreateScreenShot();
            var screenShot = manager.GetScreenShot();
            var midiControls = manager.GetMidiControls();

            await SceneManager.UnloadSceneAsync(i);
            
            var stub = stubObj.GetComponent<SubSceneStub>();
            stub.Init(rt, screenShot, i, midiControls);

            _sceneNameToStub.Add(stubObj.name, stub);
            return stubObj;
        }

        GameObject CreateSubSceneStub(string name)
        {
            var stub = new GameObject(name);
            stub.AddComponent<SubSceneStub>();
            var sender = stub.AddComponent<NdiSender>();
            sender.ndiName = name;
            sender.captureMethod = CaptureMethod.Texture;
            sender.SetResources(ndiResources);
            return stub;
        }

        RenderTexture GetRenderTexture(int i)
        {
            var rt = GetRenderTexture();
            rt.name = "RT" + i;
            return rt;
        }

        RenderTexture GetRenderTexture()
        {
            var rt = new RenderTexture(1920, 1080, 24);
            rt.Create();
            return rt;
        }

        SubSceneManager FindSubSceneManager(Scene scene)
        {
            foreach (var obj in scene.GetRootGameObjects())
            {
                var managers = obj.GetComponentsInChildren<SubSceneManager>();
                if (managers.Length > 0)
                {
                    return managers[0];
                }
            }
            
            return CreateSubSceneManager(scene);
        }

        SubSceneManager CreateSubSceneManager(Scene scene)
        {
            var previousScene = SceneManager.GetActiveScene();
            SceneManager.SetActiveScene(scene);
            
            var go = new GameObject("SubSceneManager");
            var manager = go.AddComponent<SubSceneManager>();
            manager.cameras = FindCameras(scene);
                
            SceneManager.SetActiveScene(previousScene);
            return manager;
        }

        Camera[] FindCameras(Scene scene)
        {
            foreach (var obj in scene.GetRootGameObjects())
            {
                var cameras = obj.GetComponentsInChildren<Camera>();
                if (cameras.Length > 0)
                {
                    return cameras;
                }
            }
            
            throw new Exception("NoCamerasFound Exception");
        }

        public async UniTask LoadScene(int layer, int column)
        {
            await UniTask.SwitchToMainThread();
            
            var key = layer + "." + column;
            if (_compositionIndexToSceneName.ContainsKey(key))
            {
                var sceneName = _compositionIndexToSceneName[key];
                var stub = _sceneNameToStub[sceneName];
                
                await SceneManager.LoadSceneAsync(stub.Index, LoadSceneMode.Additive);
                var scene = SceneManager.GetSceneByBuildIndex(stub.Index);
                _layerManager.SetLayer(scene, (Layers) (layer + 11));
                _loadedScenes[key] = scene;

                var manager = FindSubSceneManager(scene);
                manager.Setup(stub.RenderTexture);
                manager.InitializeInput(layer+1);
                stub.enabled = false;
            }    
        }
        
        public async UniTask UnloadScene(int layer, int column)
        {
            await UniTask.SwitchToMainThread();
            
            var key = layer + "." + column;
            if (_compositionIndexToSceneName.ContainsKey(key) && _loadedScenes.ContainsKey(key))
            {
                var sceneName = _compositionIndexToSceneName[key];
                var stub = _sceneNameToStub[sceneName];
                await SceneManager.UnloadSceneAsync(_loadedScenes[key]);
                stub.Init();

                stub.enabled = true;
            }    
        }
    }
}