using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml; 
using System.Xml.Serialization; 
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace nyc.crux.vjx.data
{
    public class ResolumeCompositionLoader
    {   
        private Regex _sourceNameRegex = new Regex(@"\(.+\)",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);
        
        public ResolumeCompositionLoader()
        {
           
        }

        public Dictionary<string, string> LoadComposition(string compositionFilePath="/Users/mike/Documents/Resolume Arena 6/Compositions/resolume-test.avc")
        {
            XElement composition = XElement.Load(compositionFilePath);

            List<Tuple<int, int, string>> clipInfos = new List<Tuple<int, int, string>>();
            var items = composition.Descendants("Clip");
            foreach (var item in items)
            {
                var clipInfo = MapClipInfo(item);
                clipInfos.Add(clipInfo);
            }
            
            return clipInfos.ToDictionary(x => x.Item1 + "." + x.Item2, x => x.Item3);
        }

        public Tuple<int, int, string> MapClipInfo(XElement item)
        {
            var layerIndex = Int32.Parse((string) item.Attribute("layerIndex"));
            var columnIndex = Int32.Parse((string) item.Attribute("columnIndex"));
            var sourceName = "Not Found";

            var videoSources = item.Descendants("VideoSource");

            foreach (var videoSource in videoSources)
            {
                var type = (string) videoSource.Attribute("type");
                if (type != null && type.Equals("NDIVideoSource"))
                {
                    sourceName = (string) videoSource.Descendants("NDIVideoInfo").First().Attribute("sourceName");
                    sourceName = CleanSourceName(sourceName);
                }
            }
            
            return new Tuple<int, int, string>(layerIndex, columnIndex, sourceName);
        }

        private string CleanSourceName(string dirtySourceName)
        {
            var match = _sourceNameRegex.Match(dirtySourceName);
            var cleanSourceName = (string) match.Value;
            return cleanSourceName.Substring(1, cleanSourceName.Length - 2); 
        }
    }
}