using nyc.crux.vjx.scenes.signals;
using nyc.crux.vjx.ui.signals;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
#pragma warning disable 1998

namespace nyc.crux.vjx.ui
{
    public class UIManager: MonoBehaviour
    {        
        public GameObject MessageTextGameObject;   
        public GameObject ResolumePathInputGameObject;
        public Text LoadButtonText;
        public Text InitText;

        private Text _messageText;
        private InputField _resolumePathInputField;

        private const string ResolumePathPlayerPrefsKey = "ResolumePath";

        private SignalBus _signalBus;
        
        [Inject]
        void Init(SignalBus signalBus)
        {
            _signalBus = signalBus;
            _signalBus.Subscribe<LoadingStartedSignal>(signal => UpdateLoadingText(signal).Forget());
            _signalBus.Subscribe<LoadingFinishedSignal>(signal => UpdateFinishedLoadingText().Forget());
            _signalBus.Subscribe<LoadingFinishedSignal>(signal => EnableResolumePathInput().Forget());
            _signalBus.Subscribe<CompositionLoadedSignal>(signal => UpdateCompositionLoadButton().Forget());
            _signalBus.Subscribe<CompositionLoadedSignal>(signal => UpdateCompositionLoadedText().Forget());
            _signalBus.Subscribe<CompositionLoadedSignal>(signal => DisableInitText().Forget());
        }

        void Start()
        {
            _messageText = MessageTextGameObject.GetComponent<Text>();
        }

        async UniTask UpdateLoadingText(LoadingStartedSignal loadingStartedSignal)
        {
            await UniTask.WaitUntil(() => _messageText != null);
            UpdateText($"Initializing Scene {loadingStartedSignal.CurrentSceneIndexLoading} of {loadingStartedSignal.TotalSceneCount}");
        }

        async UniTask UpdateFinishedLoadingText()
        {
            UpdateText($"Scenes Initialized! Waiting for Resolume Composition");
        }

        void UpdateText(string msg)
        {
            InitText.text = msg;
            _messageText.text = msg;
        }

        async UniTask EnableResolumePathInput()
        {
            ResolumePathInputGameObject.SetActive(true);
            await UniTask.WaitUntil(() => ResolumePathInputGameObject.activeInHierarchy);
            _resolumePathInputField = ResolumePathInputGameObject.GetComponentInChildren<InputField>();
            
            if (PlayerPrefs.HasKey(ResolumePathPlayerPrefsKey))
            {
                _resolumePathInputField.text = PlayerPrefs.GetString(ResolumePathPlayerPrefsKey);
            }
        }

        public void FireResolumePath()
        {
            var signal = new ResolumePathSubmittedSignal();
            signal.ResolumeCompositionPath = _resolumePathInputField.text;
            _signalBus.Fire(signal);
            PlayerPrefs.SetString(ResolumePathPlayerPrefsKey, _resolumePathInputField.text);
            PlayerPrefs.Save();
        }

        async UniTask UpdateCompositionLoadButton()
        {
            LoadButtonText.text = "Refresh";
        }

        async UniTask UpdateCompositionLoadedText()
        {
            UpdateText($"Resolume Composition Successfully Loaded");
        }

        async UniTask DisableInitText()
        {
            InitText.enabled = false;
        }
    }
}