namespace nyc.crux.vjx.ui.signals
{
    public class ResolumePathSubmittedSignal
    {
        public string ResolumeCompositionPath { get; set; }
    }
}