﻿using System.Collections.Generic;
using System.Linq;
using Klak.Ndi;
using ModestTree;
using UnityEngine;
using UnityEngine.UI;

namespace nyc.crux.vjx.ui
{
    public class NDIReceiverDropdownManager : MonoBehaviour
    {
        Dropdown _dropdown = null;

        NdiReceiver _receiver;
        List<string> _sourceNames;
        bool _disableCallback;
        private bool first = true;

        // HACK: Assuming that the dropdown has more than
        // three child objects only while it's opened.
        bool IsOpened => _dropdown.transform.childCount > 3;

        void Start()
        {
            _receiver = GetComponent<NdiReceiver>();
            _dropdown = GetComponent<Dropdown>();
        }
        
        void Update()
        {
            // Do nothing if the menu is opened.
            if (IsOpened) return;

            // NDI source name retrieval
            var refreshedSourceNames = NdiFinder.sourceNames.ToList();
            if (_sourceNames != null && _sourceNames.Equals(refreshedSourceNames))
            {
                return;
            }

            _sourceNames = refreshedSourceNames;

            // Currect selection
            var index = _dropdown.value;
            
            // Disable the callback while updating the menu options.
            _disableCallback = true;

            // Menu option update
            _dropdown.ClearOptions();
            _dropdown.AddOptions(_sourceNames);
            _dropdown.RefreshShownValue();
            _dropdown.value = index;

            // Resume the callback.
            _disableCallback = false;

            if (!_sourceNames.IsEmpty() && first)
            {
                first = false;
                OnChangeValue();
            }
        }

        public void OnChangeValue()
        {
            if (_disableCallback) return;
            var value = _dropdown.value;
            _receiver.ndiName = _sourceNames[value];
        }
    }
}