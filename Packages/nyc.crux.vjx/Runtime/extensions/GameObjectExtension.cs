using UnityEngine;

namespace nyc.crux.vjx.extensions
{
    public static class GameObjectExtension
    {
        public static void SetLayerRecursively(this GameObject self, int layer, bool ignoreSelf = false)
        {            
            if (!ignoreSelf) self.layer = layer;
            foreach (Transform t in self.transform)
            {
                SetLayerRecursively(t.gameObject, layer);
            }
        }
    }
}