﻿using System.ComponentModel;
using nyc.crux.vjx.data;
using nyc.crux.vjx.scenes;
using nyc.crux.vjx.scenes.signals;
using nyc.crux.vjx.ui.signals;
using Zenject;

namespace nyc.crux.vjx.installers
{
    public class VJInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {        
            Container.Bind<LayerManager>().AsSingle().NonLazy();
            Container.Bind<ResolumeCompositionLoader>().AsSingle().NonLazy();
        
            //Signals
            SignalBusInstaller.Install(Container);
            Container.DeclareSignal<LoadingStartedSignal>();
            Container.DeclareSignal<LoadingFinishedSignal>();
            Container.DeclareSignal<ResolumePathSubmittedSignal>();
            Container.DeclareSignal<CompositionLoadedSignal>();
        }
    } 
}