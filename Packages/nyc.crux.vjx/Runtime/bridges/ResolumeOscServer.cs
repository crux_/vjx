using System;
using System.Text.RegularExpressions;
using nyc.crux.vjx.scenes;
using OscJack;
using UniRx.Async;
using UnityEngine;
using UnityEngine.Serialization;

namespace nyc.crux.vjx.bridges
{
    public class ResolumeOscServer: MonoBehaviour
    {
        [SerializeField] 
        private MainSceneManager _mainSceneManager;
        
        [SerializeField]
        private bool debug = false;
        
        [SerializeField] 
        private int _port = 9000;
        private OscServer _sharedServer;
        
        private Regex _layerColumnRegex = new Regex(@"\d+",
        RegexOptions.Compiled | RegexOptions.IgnoreCase);
        
        private Regex _fullAddressRegex = new Regex(@"composition\/layers\/[0-9]+\/clips\/[0-9]+\/connected",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        void Start()
        {
            SetSharedServer();
            _sharedServer.MessageDispatcher.AddCallback(
                "",
                (string address, OscDataHandle data) => HandleCallback(address, data)
            );
        }

        public void SetMainSceneManager(MainSceneManager mainSceneManager)
        {
            _mainSceneManager = mainSceneManager;
        }
        
        void SetSharedServer()
        {
            _sharedServer = OscMaster.GetSharedServer(_port);
        }

        void HandleCallback(string address, OscDataHandle data)
        {
            var layerAndColumn = ParseAddress(address);
            var value = data.GetElementAsString(0);
            
            if (value.Equals("3"))
            {
                if (layerAndColumn != null)
                {
                    _mainSceneManager.LoadScene(layerAndColumn.Item1 - 1, layerAndColumn.Item2 - 1).Forget();
                }
            }else if (value.Equals("1"))
            {
                if (layerAndColumn != null)
                {
                    _mainSceneManager.UnloadScene(layerAndColumn.Item1 - 1, layerAndColumn.Item2 - 1).Forget();
                }
            }
        }
        
        
        private Tuple<int, int> ParseAddress(string address)
        {
            MatchCollection matches = _fullAddressRegex.Matches(address);

            if (matches.Count != 1)
            {
                return null;
            }
            
            matches = _layerColumnRegex.Matches(address);

            if (matches.Count == 2)
            {
                var layer = Int32.Parse(matches[0].Value);
                var column = Int32.Parse(matches[1].Value);
                
                if (debug)
                {
                    Debug.Log(address + " Layer: " + layer + " Column: " + column);
                }

                return new Tuple<int, int>(layer, column);
            }

            return null;
        }
    }
}