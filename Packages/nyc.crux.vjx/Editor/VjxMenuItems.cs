using System;
using System.Collections.Generic;
using nyc.crux.vjx.bridges;
using nyc.crux.vjx.enums;
using nyc.crux.vjx.installers;
using nyc.crux.vjx.scenes;
using UnityEditor;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace nyc.crux.vjx.Editor
{
    public class VjxMenuItems
    {
        [MenuItem("VJX/Init", false, 10)]
        static void Init(MenuCommand menuCommand)
        {
            CreateVJXManager(menuCommand);
            AddLayers();
            ModifyProjectSettings();
        }

        static void CreateVJXManager(MenuCommand menuCommand)
        {
            var objectName = "VJXManager";

            if (GameObject.FindObjectsOfType<MainSceneManager>().Length > 0)
            {
                return;
            }
            
            // Create a custom game object
            GameObject go = new GameObject(objectName);
            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;

            MainSceneManager mainSceneManager = go.AddComponent<MainSceneManager>();
            ResolumeOscServer resolumeOscServer = go.AddComponent<ResolumeOscServer>();
            resolumeOscServer.SetMainSceneManager(mainSceneManager);

            SceneContext sceneContext = go.AddComponent(typeof(SceneContext)) as SceneContext;
            VJInstaller vjInstaller = go.AddComponent(typeof(VJInstaller)) as VJInstaller;
            var installers = new List<MonoInstaller>();
            installers.Add(vjInstaller);
            sceneContext.Installers = installers;

            PlayerSettings.runInBackground = true;

            //var root = new GameObject("GameObjectContext").AddComponent<MainSceneManager>();
            //Selection.activeGameObject = root.gameObject; 
        }
        
        static void AddLayers()
        {
            Object[] asset = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset");
            if ((asset != null) && (asset.Length > 0))
            {
                SerializedObject so = new SerializedObject(asset[0]);
                SerializedProperty layers = so.FindProperty("layers");
                 
                foreach (Layers layer in (Layers[]) Enum.GetValues(typeof(Layers)))
                {
                    var index = (int) layer;
                    var name = layer.ToString();
                    if (!layers.GetArrayElementAtIndex(index).stringValue.Equals(name))
                    {
                        layers.InsertArrayElementAtIndex(index);
                        layers.GetArrayElementAtIndex(index).stringValue = name;
                    }
                }
             
                so.ApplyModifiedProperties();
                so.Update();
            }
        }

        static void ModifyProjectSettings()
        {
            var enableBoth = true;
            const string projectSettingsAssetPath = "ProjectSettings/ProjectSettings.asset";
            SerializedObject projectSettings = new SerializedObject(UnityEditor.AssetDatabase.LoadAllAssetsAtPath(projectSettingsAssetPath)[0]); 
            
            SerializedProperty enableNewInput = projectSettings.FindProperty ("enableNativePlatformBackendsForNewInputSystem");
            SerializedProperty disableOldInput = projectSettings.FindProperty("disableOldInputManagerSupport");
            enableNewInput.boolValue = enableBoth;
            disableOldInput.boolValue = !enableBoth;

            projectSettings.ApplyModifiedProperties();
        }
  
    }
}