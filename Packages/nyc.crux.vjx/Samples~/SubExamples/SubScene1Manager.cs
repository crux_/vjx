using System.Collections.Generic;
using nyc.crux.vjx.scenes;

namespace Samples.vjx._0._0._1.SubExamples
{
    public class SubScene1Manager: SubSceneManager
    {
        public override Dictionary<string, string> GetMidiControls()
        {
            var d = new Dictionary<string, string>();
            d["test"] = "value";
            return d;
        }
    }
}