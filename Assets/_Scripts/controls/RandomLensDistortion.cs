using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

namespace _Scripts.controls
{
    public class RandomLensDistortion: MonoBehaviour
    {
        [SerializeField] 
        private Volume volume;
        private LensDistortion _lensDistortion;

        void Awake()
        {
            if (volume == null)
            {
                volume = GetComponent<Volume>();
            }
            
            SetLensDistortion();
        }
        
        private void SetLensDistortion()
        {
            LensDistortion tmp;
            if(volume.profile.TryGet<LensDistortion>(out tmp) )
            {
                _lensDistortion = tmp;
            }
        }

        public void Action()
        {
            _lensDistortion.intensity.value = (Random.value - 0.5f) * 2;
        }
    }
}