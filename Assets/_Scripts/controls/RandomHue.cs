using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

namespace _Scripts.controls
{
    public class RandomHue: MonoBehaviour
    {
        [SerializeField] 
        private Volume volume;
        private ColorAdjustments _colorAdjustments;

        void Awake()
        {
            if (volume == null)
            {
                volume = GetComponent<Volume>();
            }
            
            SetColorAdjustments();
        }
        
        private void SetColorAdjustments()
        {
            ColorAdjustments tmp;
            if(volume.profile.TryGet<ColorAdjustments>(out tmp) )
            {
                _colorAdjustments = tmp;
            }
        }

        public void OnAction1()
        {
            _colorAdjustments.hueShift.value = (Random.value - 0.5f) * (Random.value * 180) ;
        }
    }
}