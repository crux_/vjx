VJX
=====
Manage Unity scenes using Resolume as an interface

![](Readme/test-footage.mp4)

Developed using
---------------------
- [KlakNDI](https://github.com/keijiro/KlakNDI)
- [Minis](https://github.com/keijiro/Minis)
- [OscJack](https://github.com/keijiro/OscJack)
- [Extenject](https://github.com/starikcetin/Extenject)

Inspired by
----------------------
- [UniVJ](https://github.com/shutosg/UniVJ)
- [Resolink](https://github.com/stella3d/Resolink)

System Requirements
----------------------
- Tested using Unity 2019.4.4f1 and Resolume Arena 6 on Windows 10 (should work on Mac, will test later)

Limitations
----------------------
- VJX makes extensive use of Unity Layers 11-17 and Layer 21. 
- Uses Extenject for dependency injection, so your Subscenes cannot use it*
- You can only use a scene **once** per Resolume Composition*

 \* could be fixed in the future

Resolume Setup
---------------------
- Create a New Composition
- Enable OSC Output in Preferences > OSC
- Make note of Server Port (can set to 9000 or something else free)

Unity Setup
---------------------
- Add the following to your project's manifest.json:

```
        "scopedRegistries": [
          {
            "name": "Keijiro",
            "url": "https://registry.npmjs.com",
            "scopes": [
              "jp.keijiro"
            ]
          },
          {
            "name": "Unity NuGet",
            "url": "https://unitynuget-registry.azurewebsites.net",
            "scopes": [
              "org.nuget"
            ]
          }
        ],
        "dependencies": {
      	"nyc.crux.vjx": "https://gitlab.com/crux_/vjx.git#upm",
      	"com.svermeulen.extenject": "https://github.com/starikcetin/Extenject.git#upm",
        "com.unity.render-pipelines.high-definition": "7.3.1",
        "jp.keijiro.klak.ndi": "1.0.0",
        "jp.keijiro.minis": "1.0.7",
        "jp.keijiro.osc-jack": "1.0.1"
      }
  ```
- Open Window > Package Manager and import VJX Main sample
- Open VJXMain
- Run VJX > Init
- In Edit > Project Settings > Player > Active Input Handling set to "Both"
- Add VJXMain as first scene in Build Settings list
- Add any other SubScenes next in list. These will be your clips in Resolume
- On ResolumeBridge prefab, set Resolume OSC Server Port

VJX Run Instructions
---------------------
- Run VJX App, wait for scenes to finish initializing  
![Step1Img](Readme/1.png "Step 1")
- In Resolume, arrange your Unity clips in composition  
![Step2Img](Readme/2.png "Step 2")  
![Step3Img](Readme/3.png "Step 3")
- In VJX, input the path to your Resolume composition and hit 'Load'  
![Step4Img](Readme/4.png "Step 4")
- Now switch back to Resolume. Your scenes can be managed and played using here. You can use VJX as an Output monitor for Resolume too.  
![Step5Img](Readme/5.png "Step 5")

Build Instructions
---------------------
- Put MainScene first in build. All SubScenes come after
- On Windows, set x86_64
- Build and run. Make sure only 1 instance of VJX is running (this includes Unity editor)
